import markdown

css = '''
<style type="text/css">
body {
	background: #dadada;
}
#container
{
	width: 60%;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	background-color: #FFFFFF;
	color: #000000;
	font-size: 14px;
	line-height: 150%;
	box-shadow: 0 0 5px #adadad;
	-moz-box-shadow: 0 0 5px #adadad;
	-webkit-box-shadow: 0 0 5px #adadad;
	margin: 15px auto;
	padding: 10px;
}
h1, h2, h3 {
	font-weight: bold;
	font-family: "Century Gothic", "Trebuchet MS";
	letter-spacing: 1pt;
	margin: 0;
	padding: 7px;
	color: teal;
}
h1 {
	font-size: 26px;
	background: lightblue;
}
h2 {
	background: lightblue;
	font-size: 22px;
}
h3 {
	font-size: 16px;
}
p {
	margin: 10px;
}
a {
	color: teal;
}
.toc {
	margin: 0;
}
.toc ul {
	padding-left: 20px;
	list-style-type: none;
}
</style>
'''

head = '''
<!DOCTYPE html>
<html>
<head>
<title>README</title>
<meta charset="UTF-8">
%s
</head>
<body>
<div id="container">
''' % css

f = open('README.md','r')
d = markdown.markdown(f.read(),extensions=['extra'])
f.close()

f = open('README.html','w')
f.write(head.lstrip())
f.write(d)
f.write('</div></body></html>')
f.close()