notewhal
========

[notewhal](http://blackwc.bitbucket.org/notewhal/) is a notifier tool. It sits in the tray and runs checks on multiple accounts, at a customizable interval, for new messages over the following protocols: IMAP4, Reddit.

Upon detection of new messages, it colors the icon in the tray a customizable color and plays a customizable sound. The icon displays new-message color-notifications for any number of accounts

Changes
-------

### 0.9.17 ###

  * added QuickLinks dialog on tray icon single left click (Windows)
  * embedded resources (img2py)
  * improved help
  * improved readme
  * updated wxPython to 2.9.4.0
  * improved about dialog
  * new icon and installer graphics
  * fixed balloon so it will not display on every check
  * fixed reddit menu items not launching
  * improved build environment
  * minor bug fixes

### 0.9.16 ###

  * added balloon notifications
  * fixed bug that played profile sounds when global sound is muted

### 0.9.15 ###

  * added logging
  * added option to suppress update checking

### 0.9.14 ###

  * fixed bug in that saved reddit profile http login when not changed

### 0.9.13 ###

  * fixed typo in new release dialog

### 0.9.12 ###

  * fixed CheckUpdate list index out of range
  * added tooltips on toolbar

### 0.9.11 ###

  * fixed bug in Reddit protocol for renaming 'Server' to 'JSON' and setting proper description

### 0.9.10 ###

  * updated to wxPython 2.9.1.1
  * fixed bug in removing profiles

### 0.9.9 ###

  * added notewhal help - documentation
  * catch exceptions in CheckMessages

### 0.9.8.1 ###

  * fixed critical bug

### 0.9.8 ###

  * improved status check frame
  * fixed typo in win32_setup.nsi and notewhal.py HTTP Login description
  * changed reddit user-agent to 'notewhal'

### 0.9.7 ###

  * improved check for new release function
  * fixed bug where a profile having connection issues cannot remove warning icon

### 0.9.6 ###

  * added icon to indicate when some profiles cannot establish login or connect

### 0.9.5 ###

  * improved exception handling
  * added ability to check for new releases

### 0.9.4 ###

  * added scroll to selected added profile or to scroll to next profile when removed

### 0.9.3 ###

  * fixed bug where the timer stopped after sleeping/hibernating

### 0.9.2 ###

  * added routine to spawn dashboard if notewhal.ini doesn't exist

### 0.9.1 ###

  * added routine to prioritize profiles (move up, move down)
  * fixed Remove profile (OnRemove) algorithm
  * added routine to check status of each profile
  * fixed coloring algorithm
  * improved gui
