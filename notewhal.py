gpl = '''notewhal - Reddit and IMAP Notifier
Copyright (C) 2010-2013 WC Black

This program is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation, either version 3 of the License, or 
(at your option) any later version.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with notewhal. If not, see <http://www.gnu.org/licenses/>.'''

import wx, os, sys, copy, threading, imaplib, platform, json, logging

from EmbeddedImages import *

import urllib2 as net
from PIL import Image, ImageOps, ImageChops
from wx import propgrid as wxpg
from string import Template
from wx.lib import newevent
from ConfigParser import ConfigParser
from version import version

#initiate logger
logging.basicConfig(filename='notewhal.log',\
	format='[%(levelname)s %(asctime)-15s] %(message)s',\
	datefmt='%d-%m-%Y %H:%M:%S')

frame = 0

#new message event
NewMessagesEvent, EVT_NEW_MESSAGES = newevent.NewEvent()

class QuickLinks(wx.Frame):

	def __init__(self, parent):
		wx.Frame.__init__(self,parent,-1,'notewhal',wx.DefaultPosition,wx.Size(200,200),wx.RESIZE_BORDER|wx.CLIP_CHILDREN|wx.FRAME_NO_TASKBAR|wx.STAY_ON_TOP)
		
		self.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))

		self.Bind(wx.EVT_ACTIVATE,self.OnDeactivate)
		
		self.activated = False
		self.toggle = False
		
	
	def OnDashboardLink(self, event):
		self.GetParent().DoDashboard()
		
	def OnCheckNowLink(self, event):
		self.GetParent().DoCheckNow()
		self.Hide()
	
	def OnDeactivate(self, event):
		self.activated = event.GetActive()
		if not self.activated: self.Hide()
		
	def OnPlink(self, event):
		wx.Execute(event.GetURL())
		
	def ShowQuickLinks(self, a):
	
		self.SetSizer(None)
		self.DestroyChildren()
		
		DashboardLink = wx.HyperlinkCtrl(self,-1,'Open Dashboard','',wx.DefaultPosition,wx.DefaultSize,wx.HL_ALIGN_CENTRE|wx.NO_BORDER)
		DashboardLink.Bind(wx.EVT_HYPERLINK,self.OnDashboardLink)
		
		CheckNowLink = wx.HyperlinkCtrl(self,-1,'Check Now','',wx.DefaultPosition,wx.DefaultSize,wx.HL_ALIGN_CENTRE|wx.NO_BORDER)
		CheckNowLink.Bind(wx.EVT_HYPERLINK,self.OnCheckNowLink)
		
		box_sizer = wx.BoxSizer(wx.VERTICAL)
		
		if a != None:
			for t, i in enumerate(a):

				if t == 0: flag = wx.TOP
				else: flag = None
				
				bar = 0			
				if i['url']: bar = i['url']
			
				box2_sizer = wx.BoxSizer(wx.HORIZONTAL)
				if flag != None:
					if bar:
						box2_sizer.Add(wx.HyperlinkCtrl(self,-1,i['name'],bar,wx.DefaultPosition,wx.DefaultSize,wx.HL_ALIGN_CENTRE|wx.NO_BORDER),0,wx.LEFT|flag,15)
					else:
						box2_sizer.Add(wx.StaticText(self,-1,i['name']),0,wx.LEFT|flag,15)
					box2_sizer.Add(wx.StaticText(self,-1,' (' + i['num'] + ')'),0,wx.RIGHT|flag,15)
					if i['program']:
						plink = wx.HyperlinkCtrl(self,-1,'P',i['program'],wx.DefaultPosition,wx.DefaultSize,wx.HL_ALIGN_CENTRE|wx.NO_BORDER)
						plink.Bind(wx.EVT_HYPERLINK,self.OnPlink)
						box2_sizer.Add(plink,0,wx.RIGHT|flag,15)
				else:
					if bar:
						box2_sizer.Add(wx.HyperlinkCtrl(self,-1,i['name'],bar,wx.DefaultPosition,wx.DefaultSize,wx.HL_ALIGN_CENTRE|wx.NO_BORDER),0,wx.LEFT,15)
					else:
						box2_sizer.Add(wx.StaticText(self,-1,i['name']),0,wx.LEFT,15)
					box2_sizer.Add(wx.StaticText(self,-1,' (' + i['num'] + ')'),0,wx.RIGHT,15)
					if i['program']:
						plink = wx.HyperlinkCtrl(self,-1,'P',i['program'],wx.DefaultPosition,wx.DefaultSize,wx.HL_ALIGN_CENTRE|wx.NO_BORDER)
						plink.Bind(wx.EVT_HYPERLINK,self.OnPlink)
						box2_sizer.Add(plink,0,wx.RIGHT,15)
					
				box_sizer.Add(box2_sizer)
				
				if t != len(a):
					box_sizer.AddSpacer(5)
		else:
			box_sizer.Add(wx.StaticText(self,-1,'No new messages.'),0,wx.EXPAND|wx.LEFT|wx.TOP|wx.RIGHT,15)
		
		box_sizer.Add(DashboardLink,0,wx.ALIGN_CENTRE_HORIZONTAL|wx.LEFT|wx.TOP|wx.RIGHT,15)
		box_sizer.AddSpacer(8)
		box_sizer.Add(CheckNowLink,0,wx.ALIGN_CENTRE_HORIZONTAL|wx.LEFT|wx.BOTTOM|wx.RIGHT,15)
		self.SetSizer(box_sizer)
		box_sizer.SetSizeHints(self)
		
		self.SendSizeEvent()
		
		panel = wx.Panel(self)
		panel.SetFocus()
		
		self.MoveIt()
		self.Raise()
		self.Show()
	
	def MoveIt(self):
		desktop = wx.Display().GetClientArea()
		desktopWidth = desktop[2]
		desktopHeight = desktop[3]
		screenWidth = wx.Display().GetGeometry()[2]
		screenHeight = wx.Display().GetGeometry()[3]
		
		taskbarHeight = screenWidth - desktopWidth
		taskbarWidth = screenHeight - desktopHeight
		
		width, height = self.GetSize()
		mouseX,mouseY = wx.GetMousePosition()

		ix = mouseX - width/2
		iy = mouseY - height/2
		
		#taskbar left
		if desktop[0] > 0:
			ix = desktop[0] + 8
		#taskbar top
		if desktop[1] > 0:
			iy = desktop[1] + 8
		#taskbar bottom
		if desktop[2] == screenWidth and desktop[0] == 0 and desktop[1] == 0:
			iy = desktop[3] - height - 8
		#taskbar right
		if desktop[3] == screenHeight and desktop[0] == 0 and desktop[1] == 0:
			ix = desktop[2] - width - 8
			
		if iy + height > screenHeight:
			iy = screenHeight - height - 8
		if ix + width > screenWidth:
			ix = screenWidth - width - 8
			
		self.Move((ix,iy))
		

class TBarIcon(wx.TaskBarIcon):
	def __init__(self, *args, **kwargs):
		wx.TaskBarIcon.__init__(self, *args, **kwargs)
		self._parent = None
	def CreatePopupMenu(self):
		self.menu = wx.Menu()
		self.buildmenu()
		return self.menu
		
	def buildmenu(self):
		for b in self.menu.GetMenuItems(): self.menu.DestroyItem(b)
	
		c = self._parent.getkeys()
		
		for a, item in enumerate(c):
			self.menu.Append(a, self._parent.data[item]['name'] + '\t' +\
			str(self._parent.profiles[item]))
			self.menu.Bind(wx.EVT_MENU,self._parent.OnTrayMenuItem,id=a)

		self.menu.AppendSeparator()
		self.menu.Append(2000,'&Check now')
		self.menu.AppendSeparator()
		self.menu.Append(3000,'&Dashboard')
		self.menu.Append(4000,'&Status')
		self.menu.AppendSeparator()
		self.menu.Append(1001,'E&xit')
		
		self.menu.Bind(wx.EVT_MENU,self._parent.OnExit, id=1001)
		self.menu.Bind(wx.EVT_MENU,self._parent.OnCheckNow, id=2000)
		self.menu.Bind(wx.EVT_MENU,self._parent.OnDashboard, id=3000)
		self.menu.Bind(wx.EVT_MENU,self._parent.OnStatus, id=4000)

class wthread(threading.Thread):
	def __init__(self,function,c,data):
		threading.Thread.__init__(self)
		self.function = function
		self.c = c
		self.data = data
		self.start()
	def run(self):
		self.function(self.c,self.data)

def CheckMessages(c,data):

	status = []
	checked_profiles = {}

	for item in c:

		NAME = ''
		PROTOCOL = ''
		try:
			NAME = data[item].setdefault('name','')
			PROTOCOL = int(data[item].setdefault('protocol',''))
		except KeyError:
			data.setdefault('')
		
		if NAME =='' or PROTOCOL == '':
			r = 'Profile has no name or a protocol is not selected.'
			logging.error(NAME + ' error -> ' + str(r))
			status.append((NAME,r))
			continue

		i = 0
		checked_profiles[item] = i
		
		if PROTOCOL == 0 or PROTOCOL == 1:

			SERVER = data[item].setdefault('server','')
			USERNAME = data[item].setdefault('username','')
			PASSWORD = data[item].setdefault('password','')
			
			if SERVER == '' or USERNAME == '' or PASSWORD == '':
				r = 'A required field is missing (e.g.: server, username, or password).'
				logging.error(NAME + ' error -> ' + str(r))
				status.append((NAME,r))
				continue
			
			try:
				if PROTOCOL == 0: imap = imaplib.IMAP4_SSL(SERVER) 
				if PROTOCOL == 1: imap = imaplib.IMAP4(SERVER)
			except Exception as r:
				logging.error(NAME + ' error -> ' + str(r))
				status.append((NAME,r))
				continue
				
			try: imap.login(USERNAME,PASSWORD)
			except imaplib.IMAP4.error as r:
				logging.error(NAME + ' error -> ' + str(r))
				status.append((NAME,r))
				continue
			
			try:
				imap.select()
				i = len(imap.search(None, 'UNSEEN')[1][0].split())
				imap.logout()
			except Exception as r:
				logging.error(NAME + ' error -> ' + str(r))
				status.append((NAME,r))

	
		elif PROTOCOL == 2:
			
			AUTH = data[item].get('server','')
			if AUTH.strip() == '':
				r = 'No token URL.'
				logging.error(NAME + ' error -> ' + str(r))
				status.append((NAME,r))
				continue
			
			foo = net.HTTPCookieProcessor()
			opener = net.build_opener(foo)
			opener.addheaders = [('User-agent', 'notewhal/%s' % version)]
			net.install_opener(opener)
			try: 
				req = opener.open(AUTH)
			except net.URLError as r:
				logging.error(NAME + ' error -> ' + str(r))
				status.append((NAME,r))
				continue

			d = req.read()
			req.close()
			
			try:
				d = json.loads(d)['data']['children']
			except Exception as r:
				d = []
				logging.error(NAME + ' error -> ' + str(r))
				status.append((NAME,r))
			i = len(d)
			
		else:
			r = 'Bad protocol.'
			logging.error(NAME + ' error -> ' + str(r))
			status.append((NAME,r))
			continue
			
		checked_profiles[item] = i

	global frame
	if frame:
		event = NewMessagesEvent(checked_profiles=checked_profiles,status=status)
		wx.PostEvent(frame, event)

class Window(wx.Frame):
	def __init__(self):
		wx.Frame.__init__(self, None,-1,'notewhal - Dashboard')
		
		self.Bind(EVT_NEW_MESSAGES, self.OnNewMessages)

		self.status = []
		self.last = 0
		self.profiles = {}
		self.data = {}
		self.dummy = {}
		self.protocols = ['IMAP4 (SSL)','IMAP4','Reddit']
		
		self.ticker = wx.Timer(self)
		self.Bind(wx.EVT_TIMER,self.OnTick)
		
		self.tray_icon = TBarIcon()
		self.tray_icon._parent = self
		
		self.tray_icon.Bind(wx.EVT_TASKBAR_LEFT_UP, self.OnTrayIconClick)
		self.tray_icon.Bind(wx.EVT_TASKBAR_LEFT_DCLICK, self.OnTrayMenuItem)
		
		if platform.system() == 'Linux': cf_name = '.notewhal'
		elif platform.system() == 'Windows': cf_name = 'notewhal.ini'
		else: cf_name = 'notewhal.ini'
		#TODO Mac OS X
		
		self.c_file = os.path.join(wx.StandardPaths_Get().GetUserConfigDir(),cf_name)
		
		#show dashboard if no General config data is present
		config = ConfigParser()
		config.read(self.c_file)
		
		show_b = False
		if len(config.sections()) <= 1: show_b = True
		
		#create and bind property gid context menu
		self.context_menu = wx.Menu()
		self.context_menu.Append(4000,'&Add profile')
		self.context_menu.Append(5000,'&Remove profile')
		self.context_menu.AppendSeparator()
		self.context_menu.Append(6000,'&Up priority')
		self.context_menu.Append(7000,'&Down priority')
		
		self.context_menu.Bind(wx.EVT_MENU,self.OnAdd,id=4000)
		self.context_menu.Bind(wx.EVT_MENU,self.OnRemove,id=5000)
		self.context_menu.Bind(wx.EVT_MENU,self.OnProfileUp,id=6000)
		self.context_menu.Bind(wx.EVT_MENU,self.OnProfileDown,id=7000)
		
		#toolbar
		global add, remove, up, down, website, about, help
		toolbar = self.CreateToolBar(wx.TB_FLAT|wx.TB_NODIVIDER)
		toolbar.AddLabelTool(200, 'Add profile',add.GetBitmap())
		toolbar.SetToolShortHelp(200,'Add profile')
		self.Bind(wx.EVT_TOOL,self.OnAdd,id=200)
		toolbar.AddLabelTool(300, 'Remove profile',remove.GetBitmap())
		toolbar.SetToolShortHelp(300,'Remove profile')
		self.Bind(wx.EVT_TOOL,self.OnRemove,id=300)
		toolbar.AddSeparator()
		toolbar.AddLabelTool(400, 'Up priority',up.GetBitmap())
		toolbar.SetToolShortHelp(400,'Up priority')
		self.Bind(wx.EVT_TOOL,self.OnProfileUp,id=400)
		toolbar.AddLabelTool(500, 'Down priority',down.GetBitmap())
		toolbar.SetToolShortHelp(500,'Down priority')
		self.Bind(wx.EVT_TOOL,self.OnProfileDown,id=500)
		toolbar.AddSeparator()
		toolbar.AddLabelTool(600,'Website',website.GetBitmap())
		toolbar.SetToolShortHelp(600,'Website')
		self.Bind(wx.EVT_TOOL,self.OnWebsite,id=600)
		toolbar.AddSeparator()
		toolbar.AddLabelTool(700,'About',about.GetBitmap())
		toolbar.SetToolShortHelp(700,'About')
		self.Bind(wx.EVT_TOOL,self.OnAbout,id=700)
		toolbar.AddLabelTool(800,'Help',help.GetBitmap())
		toolbar.SetToolShortHelp(800,'Help')
		self.Bind(wx.EVT_TOOL,self.OnHelp,id=800)
		
		toolbar.Realize()
		
		#create menu, set options, and bind events
		menubar = wx.MenuBar()
		file = wx.Menu()
		file.Append(10,'&Apply\tCtrl+S')
		self.Bind(wx.EVT_MENU,self.OnApply,id=10)
		file.AppendSeparator()
		file.Append(20,'&Close\tAlt+F4')
		self.Bind(wx.EVT_MENU,self.OnFrameClose,id=20)
		file.AppendSeparator()
		file.Append(21,'E&xit')
		self.Bind(wx.EVT_MENU,self.OnExit,id=21)
		
		help = wx.Menu()
		help.Append(30,'&Help\tF1')
		self.Bind(wx.EVT_MENU,self.OnHelp,id=30)
		help.AppendSeparator()
		help.Append(40,'&Website')
		self.Bind(wx.EVT_MENU,self.OnWebsite,id=40)
		help.AppendSeparator()
		help.Append(50,'&About')
		self.Bind(wx.EVT_MENU,self.OnAbout,id=50)
		
		edit = wx.Menu()
		edit.Append(60,'&Add profile')
		edit.Append(70,'&Remove profile')
		self.Bind(wx.EVT_MENU,self.OnAdd,id=60)
		self.Bind(wx.EVT_MENU,self.OnRemove,id=70)
		edit.AppendSeparator()
		edit.Append(80,'&Up priority')
		edit.Append(90,'&Down priority')
		self.Bind(wx.EVT_MENU,self.OnProfileUp,id=80)
		self.Bind(wx.EVT_MENU,self.OnProfileDown,id=90)
		
		menubar.Append(file,'&File')
		menubar.Append(edit,'&Edit')
		menubar.Append(help,'&Help')
		
		self.SetMenuBar(menubar)
		
		#create property grid
		self.pg = wxpg.PropertyGridManager(self,-1,style=wxpg.PG_DESCRIPTION|wxpg.PG_SPLITTER_AUTO_CENTER)
		self.pg.AddPage('Dashboard')
		self.pg.GetGrid().SetMarginColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BTNFACE))
		self.pg.GetGrid().SetCaptionBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BTNFACE))
		self.pg.GetGrid().SetCaptionTextColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BTNTEXT))
		
		#bind property grid
		self.pg.Bind(wxpg.EVT_PG_CHANGING,self.OnPGChange)
		self.pg.Bind(wxpg.EVT_PG_RIGHT_CLICK,self.OnPGRightClick)
		
		#create and bind buttons, bind some frame events, set up sizers for property grid and buttons
		panel = wx.Panel(self,-1)
		apply_btn = wx.Button(panel,-1,'Apply')
		apply_btn.Bind(wx.EVT_BUTTON,self.OnApply)
		about_btn = wx.Button(panel,-1,'About')
		close_btn = wx.Button(panel,-1,'Close')
		close_btn.Bind(wx.EVT_BUTTON, self.OnFrameClose)
		about_btn.Bind(wx.EVT_BUTTON, self.OnAbout)
		
		global version
		version_label = wx.StaticText(panel,-1,'version '+ version)
		version_label.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_GRAYTEXT))

		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(self.pg,6,wx.EXPAND)
		sizer1 = wx.BoxSizer(wx.HORIZONTAL)
		sizer1.Add(about_btn,0,wx.ALL,4)
		sizer1.Add(version_label,0,wx.ALL|wx.ALIGN_CENTER,4)
		sizer1.AddStretchSpacer()
		sizer1.Add(close_btn,0,wx.TOP|wx.BOTTOM,4)
		sizer1.Add(apply_btn,0,wx.ALL,4)
		panel.SetSizer(sizer1)
		sizer.Add(panel,0,wx.EXPAND)
		self.SetSizer(sizer)
		
		self.Bind(wx.EVT_CLOSE,self.OnFrameClose)
		
		size = (640,480)
		self.Center()
		pos = self.GetPositionTuple()
		
		if config.has_option('General','size'):
			size = config.get('General','size')
			size = tuple((int(x.strip()) for x in str(size)[1:-1].split(',')))
		if config.has_option('General','pos'):
			pos = config.get('General','pos','')
			pos = tuple((int(x.strip()) for x in str(pos)[1:-1].split(',')))
		
		c1 = open(self.c_file,'w')
		config.write(c1)
		c1.close()
		
		try:
			self.SetPosition(pos)
			self.SetSize(size)
		except Exception:
			self.SetSize((640,480))
			self.Center()
			
		self.UpdateSettings()
		
		self.dummy = copy.deepcopy(self.data)
		self.fillpropgrid('General',False)
		
		self.CheckUpdate()
		
		#set icon
		self.icon = wx.IconBundle()
		global icon
		self.icon.AddIcon(icon.GetIcon())
		self.SetIcons(self.icon)
		
		if platform.system() == 'Windows':
			self.box = QuickLinks(self)
		
		self.fshow = show_b
		self.Show(self.fshow)
	
	#event to catch single left click
	def OnTrayIconClick(self, event):
		if platform.system() == 'Windows':
			if self.box.toggle:
				self.box.toggle = True
				self.box.Lower()
			else:
				self.box.toggle = False
				self.box.ShowQuickLinks(self.makeboxinfo())
		else:
			self.box.toggle = False
			self.box.ShowQuickLinks(self.makeboxinfo())
	
	#event to launch CHM help file
	def OnHelp(self, event):
		wx.LaunchDefaultBrowser(os.path.join(os.getcwd(), 'help.html'))
		
	def OnTextUrl(self, event):
		if event.GetMouseEvent().ButtonDown():
			wx.LaunchDefaultBrowser(str(event.GetEventObject().GetRange(event.GetURLStart(),event.GetURLEnd())))
	
	#event to launch about message
	def OnAbout(self, event): 
		global version, gpl, about_banner

		ab = wx.Dialog(self, wx.ID_ANY, 'About notewhal',wx.DefaultPosition,(345,345))
		ab.Bind(wx.EVT_TEXT_URL,self.OnTextUrl)

		about_banner_bmp = wx.StaticBitmap(ab,bitmap=about_banner.GetBitmap())
		
		notebook = wx.Notebook(ab)
		
		version_info = '''notewhal %s
<http://blackwc.bitbucket.org/notewhal/>

wxPython %s
<http://wxpython.org>

Python %s
<http://python.org>''' % (version, wx.VERSION_STRING, '.'.join([str(sys.version_info.major),str(sys.version_info.minor),str(sys.version_info.micro)]))

		version_panel = wx.Panel(notebook)
		version_text = wx.TextCtrl(version_panel,wx.ID_ANY,version_info,wx.DefaultPosition,wx.DefaultSize,wx.TE_AUTO_URL|wx.TE_READONLY|wx.TE_MULTILINE)
		version_sizer = wx.BoxSizer(wx.HORIZONTAL)
		version_sizer.Add(version_text,1,wx.EXPAND|wx.ALL,4)
		version_panel.SetSizer(version_sizer)
		
		cred_info = '''blackwc <http://blackwc.bitbucket.org>:
 - Developer

FatCow <http://fatcow.com/free-icons/>:
 - Toolbar Icons

wxWidgets <http://wxwidgets.org>:
 - GUI Library

wxPython <http://wxpython.org>:
 - wxWidgets wrapper for Python

Python <http://python.org>:
 - Interpreted Programming Language'''

		cred_panel = wx.Panel(notebook)
		cred_text = wx.TextCtrl(cred_panel,wx.ID_ANY,cred_info,wx.DefaultPosition,wx.DefaultSize,wx.TE_AUTO_URL|wx.TE_READONLY|wx.TE_MULTILINE)
		cred_sizer = wx.BoxSizer(wx.HORIZONTAL)
		cred_sizer.Add(cred_text,1,wx.EXPAND|wx.ALL,4)
		cred_panel.SetSizer(cred_sizer)
		
		gpl_panel = wx.Panel(notebook)
		gpl_list = gpl.split('\n\n')
		gpl_format = '\n'.join(gpl_list[:1]) + '\n\n' + '\n\n'.join([x.replace('\n','') for x in gpl_list[1:]])
		gpl_text = wx.TextCtrl(gpl_panel,wx.ID_ANY,gpl_format,wx.DefaultPosition,wx.DefaultSize,wx.TE_AUTO_URL|wx.TE_READONLY|wx.TE_MULTILINE)
		gpl_sizer = wx.BoxSizer(wx.HORIZONTAL)
		gpl_sizer.Add(gpl_text,1,wx.EXPAND|wx.ALL,4)
		gpl_panel.SetSizer(gpl_sizer)
		
		notebook.AddPage(version_panel, 'Version')
		notebook.AddPage(cred_panel, 'Credits')
		notebook.AddPage(gpl_panel, 'License')
		
		vsizer = wx.BoxSizer(wx.VERTICAL)
		vsizer.Add(about_banner_bmp)
		vsizer.Add(notebook,1,wx.ALL|wx.EXPAND,4)
		ab.SetSizer(vsizer)
		
		ab.Center()
		ab.ShowModal()
	
	#event to launch project website
	def OnWebsite(self, event): wx.LaunchDefaultBrowser('http://blackwc.bitbucket.org/notewhal/')

	#event to catch propertgrid right clicks
	def OnPGRightClick(self, event):
		cat = event.GetPropertyName().rsplit('_',1)[0]
		self.pg.PopupMenu(self.context_menu,self.pg.ScreenToClient(wx.GetMousePosition()))
	
	#function to read config file into dictionary
	def loaddata(self):
	
		self.data.clear()
		
		config = ConfigParser()
		config.read(self.c_file)
		
		if not config.has_section('General'): config.add_section('General')
		
		if not config.has_option('General','frequency'):
			config.set('General','frequency','5')
		if not config.has_option('General','icon'):
			config.set('General','icon',os.path.join(os.getcwd(),'res','email.png'))
		else:
			if not os.path.exists(config.get('General','icon')):
				config.set('General','icon',os.path.join(os.getcwd(),'res','email.png'))
		if not config.has_option('General','sound'):
			config.set('General','sound',os.path.join(os.getcwd(),'res','notify.wav'))
		else:
			if not os.path.exists(config.get('General','sound')):
				config.set('General','sound',os.path.join(os.getcwd(),'res','notify.wav'))
		if not config.has_option('General','checkupdate'):
			config.set('General','checkupdate','True')
		if platform.system() == 'Windows':
			if not config.has_option('General','balloon'):
				config.set('General','balloon','True')
		
		for section in config.sections():
				a = {}
				for item in config.items(section):
					a[item[0]] = str(item[1])
				self.data[section] = a

	#event to add new profile
	def OnAdd(self, event):

		c = self.dummy.keys()
		c = self.sortthem(c)
		
		e = 'profile'
		
		if c: e += str(len(c))
		else: e += '0'

		self.dummy[e] = {}
		
		self.dummy[e]['name'] = 'new profile'
		self.dummy[e]['server'] = ''
		self.dummy[e]['loginserv'] = ''
		self.dummy[e]['protocol'] = 0
		self.dummy[e]['program'] = ''
		self.dummy[e]['sound'] = ''
		self.dummy[e]['username'] = ''
		self.dummy[e]['password'] = ''
		self.dummy[e]['color'] = '(0,0,255)'
		
		self.fillpropgrid(e,False)
	
	#event to remove profile
	def OnRemove(self, event):
		
		cat = self.pg.GetPropertyName(self.pg.GetGrid().GetSelectedProperty()).rsplit('_',1)[0]
		if cat == 'General' or cat.strip() == '': return
		
		del self.dummy[cat]
		
		data = self.sortthem(self.dummy.keys())
		d = {}
		
		for item in reversed(data):
			num = int(item.rsplit('e')[1])
			if num > int(cat.rsplit('e')[1]):
				d.setdefault('profile'+str(num-1),self.dummy[item])
				del self.dummy[item]
				
		self.dummy.update(d)
		
		bar = int(cat.rsplit('e')[1])
		if bar >= len(data): bar -= 1
		foo = 'profile'+str(bar)
		if bar < 0: foo = 'General'
		
		self.fillpropgrid(foo, False)
	
	#event to move profile up in property grid and priority
	def OnProfileUp(self, event):
		
		cat = self.pg.GetPropertyName(self.pg.GetGrid().GetSelectedProperty()).rsplit('_',1)[0]
		if cat == 'General' or cat.strip() == '': return
		
		num = int(cat.rsplit('e')[1])
		
		if num <= 0: return #return if current profile is the maximum priority
		
		data = copy.deepcopy(self.dummy['profile'+str(num)])
		
		self.dummy['profile'+str(num)].update(self.dummy['profile'+str(num-1)])
		self.dummy['profile'+str(num-1)].update(data)
		
		self.fillpropgrid('profile'+str(num-1),False)
	
	#event to move profile down in property grid and priority
	def OnProfileDown(self, event):
		
		cat = self.pg.GetPropertyName(self.pg.GetGrid().GetSelectedProperty()).rsplit('_',1)[0]
		if cat == 'General' or cat.strip() == '': return
		
		l = len(self.dummy.keys())-2
		num = int(cat.rsplit('e')[1])
		
		if num >= l: return #return if current profile is the minimum priority
		
		data = copy.deepcopy(self.dummy['profile'+str(num)])
		
		self.dummy['profile'+str(num)].update(self.dummy['profile'+str(num+1)])
		self.dummy['profile'+str(num+1)].update(data)
		
		self.fillpropgrid('profile'+str(num+1),False)
	
	#event to catch property grid value changes
	def OnPGChange(self,event):
		val = event.GetValue()
		cat = event.GetPropertyName().rsplit('_',1)[0]
		name = event.GetPropertyName().rsplit('_',1)[1]
		
		b = []

		c = self.dummy.keys()
		c = self.sortthem(c)
		
		for x in c:
			b.append(self.dummy[x]['name'])
		b.append('General')

		if name == 'name':
			if val in b:
				self.pg.Freeze()
				self.pg.RefreshGrid()
				self.pg.Thaw()
				event.Veto()
				return
			else:
				self.pg.SetPropertyLabel(cat,val)
		if name == 'protocol':
			if val == 2:
				#self.pg.HideProperty(cat + '_password',True)
				self.pg.SetPropertyValue(cat + '_loginserv','http://reddit.com')
				self.dummy[cat]['loginserv'] = 'http://reddit.com'
				self.pg.SetPropertyLabel(cat + '_server','JSON')
				self.pg.SetPropertyHelpString(cat + '_server','The JSON auth URL for unread messages and located in your reddit.com preferences.')
			else:
				self.pg.SetPropertyHelpString(cat + '_server','The URL to the server for this profile.')
				self.pg.SetPropertyLabel(cat + '_server','Server')
				#self.pg.HideProperty(cat + '_password',False)

		self.dummy[cat][name] = str(val)
		
		self.pg.Freeze()
		self.pg.RefreshGrid()
		self.pg.Thaw()
	
	#function to get sorted keys
	def getkeys(self):
		foo = self.data.keys()
		foo = self.sortthem(foo)
		return foo
	
	#function to update the settings
	def UpdateSettings(self):
		
		#load data from config file into dictionary
		self.loaddata()

		#read new data
		freq = float(self.data['General']['frequency'])
		icon = self.data['General']['icon']
		self.gsound = self.data['General']['sound']
		
		#stop timer
		if self.ticker.IsRunning(): self.ticker.Stop()
	
		#prepare icon
		self.source = Image.open(icon, 'r')

		#get get keys ordered
		c = self.getkeys()
		self.profiles.clear()
		for item in c: self.profiles[item] = 0
		
		#set tooltip and icon
		ico = wx.EmptyIcon()
		ico.CopyFromBitmap(wx.Bitmap(icon))
		self.tray_icon.SetIcon(ico,self.maketooltip())
		
		#restart timer
		if float(freq) < 0.5: freq = 0.5
		else: freq = float(freq)
		self.ticker.Start(freq*60000)
		
		#run check now
		wx.CallAfter(self.DoCheckNow)
	
	#function to sort keys
	def sortthem(self,tosort):
		b = []
		if('General' in tosort): tosort.remove('General')
		for x in tosort:
			if x:
				b.append(int(filter(type(x).isdigit, x)))
		b.sort()
		return ['profile'+str(x) for x in b]
	
	#event to run a check on each timer tick
	def OnTick(self,event): self.DoCheckNow()
	
	#function to save dictionary to config file
	def dosave(self):
		
		self.data = copy.deepcopy(self.dummy)
	
		config = ConfigParser()
		
		for section in self.data.keys():
			if not config.has_section(section): config.add_section(section)
			for item in self.data[section].items():
				config.set(section,item[0],str(item[1]))

		c1 = open(self.c_file,'w')
		config.write(c1)
		c1.close()

	#event to save and apply settings
	def OnApply(self, event):
		
		self.dosave()
		self.UpdateSettings()
		self.Close()

	#function to update the property grid
	def fillpropgrid(self,sel,revert):
		
		if revert: self.loaddata()
		
		c = self.dummy.keys()
		c = self.sortthem(c)
		c.insert(0,'General')
		
		options = {}
		
		self.pg.Freeze()
		self.pg.GetGrid().Clear()

		self.pg.Append(wxpg.PropertyCategory('General'))
		
		for item in c:
			
			if item != 'General':
				self.pg.Append(wxpg.PropertyCategory(self.dummy[item].setdefault('name','new profile'),item))
			
			for x in self.dummy[item]: options[x] = self.dummy[item][x]
			
			if item == 'General':
				
				self.pg.Append(wxpg.FloatProperty('Frequency',item + '_frequency',value=float(options.setdefault('frequency',5))))
				self.pg.SetPropertyHelpString(item + '_frequency','The interval between checks in minutes. Minimum is 0.5 (30 seconds).')
				self.pg.SetPropertyAttribute(item + '_frequency','Min',0.5)
				
				val = options.setdefault('sound',os.path.join(os.getcwd(),'res','notify.wav'))
				self.pg.Append(wxpg.FileProperty('Sound',item + '_sound',value=val))
				self.pg.SetPropertyHelpString(item + '_sound','The sound that plays when messages are detected. This sound plays for any profiles with its own sound property left blank. To disable sound globally, enter "#".')
				
				val = options.setdefault('icon',os.path.join(os.getcwd(),'res','email.png'))
				self.pg.Append(wxpg.FileProperty('Icon',item + '_icon',value=val))
				self.pg.SetPropertyHelpString(item + '_icon','The icon that sits in the tray and changes colors to notify you of new messages.')
				
				if platform.system() == 'Windows':
					val = options.setdefault('balloon','True')
					if val == 'True': val = True
					else: val = False
					self.pg.Append(wxpg.BoolProperty('Display Balloon',item+'_balloon',value=val))
					self.pg.SetPropertyHelpString(item+'_balloon','If True, show balloon notifications.')
				
				val = options.setdefault('checkupdate','True')
				if val == 'True': val = True
				else: val = False
				self.pg.Append(wxpg.BoolProperty('Update Checks',item + '_checkupdate',value=val))
				self.pg.SetPropertyHelpString(item + '_checkupdate','If True, run a check for updates at each application start.')
				
			else:
			
				self.pg.Append(wxpg.StringProperty('Name',item + '_' + 'name',value=options.setdefault('name','new profile')))
				self.pg.SetPropertyHelpString(item + '_name','The name of this profile.')
				
				self.pg.Append(wxpg.EnumProperty('Protocol',item + '_protocol',self.protocols,[0,1,2],int(options.setdefault('protocol',0))))
				self.pg.SetPropertyHelpString(item + '_protocol','The protocol for this profile.')
				
				self.pg.Append(wxpg.StringProperty('Username',item + '_username',value=options.setdefault('username','')))
				self.pg.SetPropertyHelpString(item + '_username','The name you use to log into your account for this profile - sometimes the domain is required (e.g., username@example.com). The username is not required for the Reddit protocol but can be used for HTTP Login or Program input options.')
				
				self.pg.Append(wxpg.StringProperty('Password',item + '_password',value=options.setdefault('password','')))
				self.pg.SetPropertyAttribute(item + '_' + 'password', 'Password', 1)
				self.pg.SetPropertyHelpString(item + '_password','The password for this profile. \n\nREDDIT: Please use the JSON option for logging into Reddit. The password is not used for logging into Reddit but can be used for HTTP Login or Program input options.')
				
				if int(options.setdefault('protocol',0)) == 2:
					self.pg.Append(wxpg.StringProperty('JSON',item + '_server',value=options.setdefault('server','')))
					self.pg.SetPropertyHelpString(item + '_server','The JSON auth URL for unread messages and located in your reddit.com preferences.')
				else:
					self.pg.Append(wxpg.StringProperty('Server',item + '_server',value=options.setdefault('server','')))
					self.pg.SetPropertyHelpString(item + '_server','The URL to the server for this profile.')
				
				self.pg.Append(wxpg.LongStringProperty('HTTP Login',item + '_loginserv',value=options.setdefault('loginserv','')))
				self.pg.SetPropertyHelpString(item + '_loginserv','The URL where you log in to check your messages via HTTP. You may leave this blank if you do not require the functionality.\n\n$username is a template for your username and $password is a template for your password.')
				
				if int(options.setdefault('protocol',0)) == 2:
					if options.get('loginserv','') == '': self.pg.SetPropertyValue(item + '_loginserv','http://reddit.com')
				
				self.pg.Append(wxpg.FileProperty('Program',item + '_program',value=options.setdefault('program','')))
				self.pg.SetPropertyHelpString(item + '_program','The program to launch when you select the profile. You may leave this blank if you do not require the functionality.\n\n$username is a template for your username and $password is a template for your password. The templates may be used as program arguments.')
				
				self.pg.Append(wxpg.FileProperty('Sound',item + '_sound',value=options.setdefault('sound','')))
				self.pg.SetPropertyHelpString(item + '_sound','The sound that plays for this profile when new messages are detected. To disable sound for this profile when a global sound is set, enter "#".')
				
				rgb = tuple((int(x.strip()) for x in str(options.setdefault('color','(0,0,255)'))[1:-1].split(',')))
				self.pg.Append(wxpg.ColourProperty('Color',item + '_color',value=rgb))
				self.pg.SetPropertyHelpString(item + '_color','The color that the icon for this profile changes to when new new messages are detected.')
				
				self.pg.SetPropertyHelpString(item,'Priority: ' + filter(type(item).isdigit, item))
				
		self.pg.SetPropertyHelpString('General','These settings apply to all profiles.\n\nRight-click a cell for options to add/delete profiles.')

		if sel:
			self.pg.CollapseAll()
			if not self.pg.IsPropertyExpanded(sel): self.pg.Expand(sel)
			self.pg.Expand('General')
			self.pg.SelectProperty(sel)
			self.pg.GetGrid().Scroll(-1,self.pg.GetSelectedProperty().GetY()/self.pg.GetGrid().GetRowHeight())
		
		self.pg.RefreshGrid()
		self.pg.Thaw()
	
	#event to display the dashboard window
	def DoDashboard(self):
		self.dummy = copy.deepcopy(self.data)
		self.fillpropgrid('General',True)
		
		self.fshow^=1
		if not self.fshow: self.fillpropgrid(0,True)
		self.Show(self.fshow)
	def OnDashboard(self,event):
		self.DoDashboard()

	#event to catch the dashboard close event
	def OnFrameClose(self,event):
		
		self.dummy.clear()

		self.fshow = 0
		self.Show(self.fshow)
	
	#event to catch the click of tray icon
	def OnTrayMenuItem(self,event):
	
		id = event.GetId()
		c = self.getkeys()
		
		if id == -1: id = self.last
		
		loginserv = self.data[c[id]].get('loginserv','')
		program = self.data[c[id]].get('program','')
		user = self.data[c[id]]['username'].split('@',1)[0]
		passw = self.data[c[id]].get('password','')
														
		if loginserv:
			wx.LaunchDefaultBrowser(loginserv)
		if program:
			program = Template(program).safe_substitute(username=user,password=passw)
			wx.Execute(program)
			
		self.last = id #TODO: needs testing

	#function to launch thread to check a profile
	def DoCheckNow(self): wthread(CheckMessages,self.getkeys(),self.data)

	#event to catch the Check now option in the tray context menu
	def OnCheckNow(self,event): self.DoCheckNow()
	
	#function to build the tray icon tooltip
	def maketooltip(self):
		a = ''
		c = self.profiles.keys()
		c = self.sortthem(c)

		for x in c:
			if self.profiles[x] != 0:
				a += self.data[x]['name'] +': '+str(self.profiles[x])+'\n'
		a = a.rstrip()
		if not a: a = 'No new messages'
		
		return a
		
	def makeboxinfo(self):
		a = []
		c = self.profiles.keys()
		c = self.sortthem(c)
		
		for x in c:
			if self.profiles[x] == 0: continue
			a.append({'name':self.data[x]['name'],'program': self.data[x]['program'], 'url':self.data[x]['loginserv'],'num':str(self.profiles[x])})
		
		if not a: a = None
		return a
	
	#event to catch new message event
	def OnNewMessages(self, event):
	
		#check if there is no frame (ie is shutting down and frame id destroyed)
		global frame
		if not frame: return
	
		c = self.getkeys()
		checked_profiles = event.checked_profiles
		self.status = event.status
		foo = []

		#check flag for balloon display
		if platform.system() == 'Windows':
			check_flag = False
		
		#play the sound
		for a, item in enumerate(c):

			i = checked_profiles.get(item,0)
			l = self.profiles[item]
			
			try:
				SOUND = self.data[item].setdefault('sound','')
				NAME = self.data[item].setdefault('name','')
			except Exception: continue

			if l != i and l < i and self.gsound != '#':
				if SOUND != '' and SOUND != '#':
					snd = wx.Sound(SOUND)
					if snd.IsOk(): snd.Play(wx.SOUND_ASYNC)
				if SOUND.strip() == '':
					snd = wx.Sound(self.gsound)
					if snd.IsOk(): snd.Play(wx.SOUND_ASYNC)
			
			#logic for balloon display -------------
			if platform.system() == 'Windows':
				if l != i and l < i:
					check_flag = True
			#-------------------------------------
					
			if l != i:
				self.profiles[item] = i
			
			if i:
				self.last = int(item.rsplit('e')[1])
				foo.append(item)
		
		
		#display balloon (WINDOWS ONLY) ---------------------------
		if platform.system() == 'Windows':
			display_balloon = True
			
			if 'General' in self.data:
				if 'balloon' in self.data['General']:
					if self.data['General']['balloon'] == 'False':
						display_balloon = False
			
			if display_balloon and check_flag:
							
				msg = ''
				c = self.profiles.keys()
				c = self.sortthem(c)

				for x in c:
					if self.profiles[x] != 0:
						msg += self.data[x]['name'] +': '+str(self.profiles[x])+'\n'
						
				if msg:
					msg = msg.rstrip()
					self.tray_icon.ShowBalloon('',msg)
		# ---------------------------------------------------------
		
		
		#color the tray icon
		d = self.source.copy()
		for i, x in enumerate(foo):
		
			COLOR = self.data[x]['color']
			rgb = tuple((int(x.strip()) for x in str(COLOR)[1:-1].split(',')))
			w,h = self.source.size

			b = Image.new('RGBA',(w,h),rgb)
			
			c = (0,0)

			if i == 0:
				d = ImageChops.lighter(self.source,b)
			elif i == 1:
				t = (0,0,w,h/2)
				e = ImageChops.lighter(self.source,b).crop(t)
				d.paste(e,(0,0))
			elif i == 2:
				t = (0,0,w/2,h/2)
				e = ImageChops.lighter(self.source,b).crop(t)
				d.paste(e,(c[0],c[1]))
			elif i == 3:
				t = (w/2,h/2,w,h)
				c = (w/2,h/2)
				e = ImageChops.lighter(self.source,b).crop(t)
				d.paste(e,(c[0],c[1]))

		if self.status:
			n = Image.open(os.path.join(os.getcwd(),'res','error.png'),'r')
			d.paste(n,(0,self.source.size[1]-n.size[1]-2))
		
		#prepare the icon
		img = apply(wx.EmptyImage, d.size)
		img.SetData(d.convert('RGB').tostring())
		img.SetAlphaData(self.source.convert('RGBA').tostring()[3::4])
		img = img.ConvertToBitmap()
		
		#set the icon
		ico = wx.EmptyIcon()
		ico.CopyFromBitmap(img)
		self.tray_icon.SetIcon(ico,self.maketooltip())

	#event to catch the exit
	def OnExit(self, event): wx.CallAfter(self.DoExit)
	
	#function to clean up before exit
	def DoExit(self):
	
		#write size and position of dashboard window
		config = ConfigParser()
		config.read(self.c_file)
		if not config.has_section('General'): config.add_section('General')
		config.set('General','size', str(self.GetSizeTuple()))
		config.set('General','pos', str(self.GetPositionTuple()))
		
		c1 = open(self.c_file,'w')
		config.write(c1)
		c1.close()
		
		#stop the timer
		if self.ticker.IsRunning(): self.ticker.Stop()
		#unbind the new message event
		self.Bind(EVT_NEW_MESSAGES, None)
		
		#destroy the tray icon and window
		self.tray_icon.Destroy()
		self.Destroy()
	
	#event to check the status
	def OnStatus(self, event):
	
		#if there are no issues, then display OK message
		if not self.status: 
			wx.MessageBox('All profiles are OK!', 'notewhal - Status')
			return
			
		#else, create status status frame and display status messages
		status_frm = wx.Dialog(self,-1,'notewhal - Status',wx.DefaultPosition,wx.Size(300,190),wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER)
		
		sizer = wx.BoxSizer(wx.VERTICAL)
		list = wx.ListView(status_frm)

		list.InsertColumn(0,'Profile')
		list.InsertColumn(1,'Error',wx.LIST_FORMAT_LEFT,175)

		for a, i in enumerate(self.status):
			b = list.InsertStringItem(a,unicode(i[0]))
			list.SetStringItem(b,1,unicode(i[1]))
		
		sizer1 = wx.BoxSizer(wx.HORIZONTAL)
		sizer1.Add(list,1,wx.ALL|wx.EXPAND,4)
		sizer.Add(sizer1,wx.EXPAND)
		status_frm.SetSizer(sizer1)
		status_frm.Center()
		status_frm.ShowModal()
	
	#function to check for new updates on application start
	def CheckUpdate(self):
	
		#if check for updates is set to false, then do not check for updates
		if 'General' in self.data:
			if 'checkupdate' in self.data['General']:
				if self.data['General']['checkupdate'] == 'False': return
		
		#else, check for updates
		
		#try to retrieve latest version file
		try: req = net.urlopen('https://bitbucket.org/blackwc/notewhal/raw/master/version.py')
		except net.URLError as r: 
			logging.error('notewhal error -> ' + str(r))
			return
		
		#if retrieved, read file data and compare to current version
		d = req.read()
		req.close()
		
		d = d.split('=')[1]
		d = d.strip()
		d = d.strip("'")
		
		c_v = d
		global version
		t_v = version
		
		c_v = [int(x) for x in c_v.split('.')]
		t_v = [int(x) for x in t_v.split('.')]
		
		if len(c_v) > len(t_v):
			t_v = t_v + [0]*(len(c_v) - len(t_v))
		if len(t_v) > len(c_v):
			c_v = c_v + [0]*(len(t_v) - len(c_v))
		
		#if latest version is greater than current version, display message
		for i, x in enumerate(t_v):
			if x < c_v[i]:
				a = wx.MessageBox('A new version of notewhal is available.\n\nWould you like to update?','notewhal',wx.ICON_QUESTION|wx.YES_NO)
				#if user wishes to update then launch program homepage
				if a == wx.YES: wx.LaunchDefaultBrowser('http://blackwc.bitbucket.org/notewhal/')
				break

app = wx.App()
frame = Window()
app.MainLoop()
