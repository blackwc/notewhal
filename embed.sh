rm -f ./EmbeddedImages.py
echo "from wx.lib.embeddedimage import PyEmbeddedImage" >> ./EmbeddedImages.py
mkdir -p ./tmp-res/

for f in ./res/*.*
do

	if [ "$f" == "./res/email.png" ] || [ "$f" == "./res/notify.wav" ]; then
		continue
	fi

		echo Processing $f
		$1 $f ./tmp-res/$(exec basename ${f%.*}).py >/dev/null 2>&1
		
		rm -f ./tmp-res/email.py
		rm -f ./tmp-res/notify.wav
		if [ -f ./tmp-res/$(exec basename ${f%.*}).py ]
			then
				tail --lines=+5 ./tmp-res/$(exec basename ${f%.*}).py >> ./EmbeddedImages.py
		fi
	
done

rm -rf ./tmp-res/
