XSLTPROC=xsltproc
7z=C:/Program Files/7-Zip/7z.exe
PYINSTALLER=C:/tools/pyinstaller-2.0/utils/
EMBEDDER=img2py

#if windows
NSIS=C:/Program Files/NSIS/makensis.exe
#endif

VERSION=0.9.17

.PHONY: help clean

all: clean source binary setup

setup: clean _setup

_setup: embed _binary help clean
	#if windows
	-mkdir ./build-$(VERSION)
	"$(NSIS)" -DPRODUCT_VERSION="$(VERSION)" ./win32_setup.nsi
	mv ./notewhal-$(VERSION)_win32_setup.exe ./build-$(VERSION)
	#endif
	
binary: clean embed help _binary

	#if windows
	mkdir -p ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/res/
	
	cp ./res/email.png ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/res/
	cp ./res/notify.wav ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/res/
	cp ./help.html ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/
	cp ./README.html ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/
	cp ./LICENSE.txt ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/
	
	cp ./dist/notewhal/*.* ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/
	
	"$(7z)" a -tzip ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin.zip ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/notewhal-$(VERSION)_win32_bin/
	
	rm -rf ./build-$(VERSION)/notewhal-$(VERSION)_win32_bin/
	#endif
	
_binary:

	#if windows
	python -O "$(PYINSTALLER)/Makespec.py" --windowed --icon=res/icon.ico --name=notewhal notewhal.py
	python -O "$(PYINSTALLER)/Build.py" ./notewhal.spec
	#endif
	
source: clean embed _source

_source:

	rm -rf ./build-$(VERSION)/notewhal-$(VERSION)_src/
	rm -rf ./build-$(VERSION)/notewhal-$(VERSION)_src.zip
	
	mkdir -p ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src/res/
	mkdir -p ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src/res/setup/
	mkdir -p ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src/res/ico/
	
	-cp ./* -t ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src/
	cp ./res/*.* ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src/res/
	cp ./res/setup/*.* ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src/res/setup/
	cp ./res/ico/*.* ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src/res/ico/

	"$(7z)" a -tzip ./build-$(VERSION)/notewhal-$(VERSION)_src.zip ./build-$(VERSION)/notewhal-$(VERSION)_src/notewhal-$(VERSION)_src
	
	rm -rf ./build-$(VERSION)/notewhal-$(VERSION)_src/
	
help:
	python make_help.py
	python make_readme.py
	
embed:
	sh embed.sh $(EMBEDDER)
clean:

	rm -f *.html
	rm -rf ./build-*/
	rm -rf ./build/
	rm -rf ./dist/
	rm -f *.pyc
	rm -f *.pyo
	rm -f *.spec
	rm -f warnnotewhal.txt
	rm -f *.log