notewhal
========

[TOC]

Introduction
------------

[notewhal](http://blackwc.bitbucket.org/notewhal/) is a notifier tool. It sits in the tray and runs checks on multiple accounts, at a customizable interval, for new messages over the following protocols: IMAP4, Reddit.

Upon detection of new messages, it colors the icon in the tray a customizable color and plays a customizable sound. The icon displays new-message color-notifications for any number of accounts

Getting Started
---------------

### Adding a Profile ###

Profiles can be added in three different ways:
  
  * Click the add profile button on the toolbar.
  * Right click a cell and select "Add profile."
  * Click "Edit" on the main menu, and then click the "Add profile" item.
  
### Removing a Profile ###

Profiles can be removed in three different ways:

  * Select the profile you wish to remove, and then click the remove profile button on the toolbar.
  * Right click a cell of the profile you wish to remove, and then select "Remove profile."
  * Select the profile you wish to remove, click "Edit" on the main menu, and then click the "Remove profile" item.

### Gmail and IMAP ###

Select the IMAP4 protocol with or without SSL. Set the server to the location of the IMAP server (e.g., imap.example.com).

The Gmail IMAP server is imap.gmail.com.

When using Google Apps, make sure the username is such: username**@example.com**

### Reddit ###

Select the Reddit protocol. Set the JSON field to the JSON URL for inbox unread messages. This URL can be found by navigating to reddit.com, logging in, choosing preferences, locating the "RSS Feeds" option.

Feedback
--------

### Issues ###

Bugs may be reported and features requested at the [issue tracker](http://bitbucket.org/blackwc/notewhal/issues).